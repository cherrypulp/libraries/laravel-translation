<?php

namespace Blok\Translation\Exceptions;

class LanguageKeyExistsException extends \Exception
{
}
