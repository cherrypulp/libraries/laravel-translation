<?php namespace Blok\Translation\Providers;

use Blok\Utils\Arr;
use Illuminate\Translation\Translator as LaravelTranslator;
use Illuminate\Events\Dispatcher;
use Blok\Translation\Translation;

class Translator extends LaravelTranslator
{

    /** @var  Dispatcher */
    protected $events;

    public static $_translations;

    /**
     * Get the translation for the given key.
     *
     * @param string $key
     * @param array $replace
     * @param string $locale
     * @return string
     */
    public function get($key, array $replace = array(), $locale = null, $fallback = true)
    {
        $result = null;

        // Check data inside translations table
        if (config('translation.driver') === 'database') {

            $translations = self::loadTranslationsFromDatabase();

            if (!$locale) {
                $locale = config('app.locale');
            }

            $fallbackLocale = config('app.fallback_locale');

            if (isset($translations[$locale][$key])) {
                $result = $translations[$locale][$key];
            }

            if (!$result && $fallback && isset($translations[$fallbackLocale][$key])) {
                $result = $translations[$fallbackLocale][$key];
            }

            /**
             * If no result => we must search for loose key or an array
             */
            if (!$result) {
                $results = collect($translations[$locale])->filter(function ($v, $k) use ($key) {
                    return preg_match("/^$key\./", $k);
                });

                if (!$results && $fallback && isset($translations[$fallbackLocale])) {
                    $results = collect($translations[$fallbackLocale])->filter(function ($v, $k) use ($key) {
                        return preg_match("/^$key\./", $k);
                    });
                }

                if ($results->count()) {
                    $result = Arr::dot_array($results->keyBy(function ($v, $k) use($key) {
                        return str_replace($key . '.', '', $k);
                    })->toArray());
                }
            }

            if ($result) {
                /**
                 * Dirty hack to make the same replacement logic than Laravel Translator
                 *
                 * Adding a unique ref make the replacement working by removing it right after that ($^^$ is used to add a unique key ref that can't exist
                 */
                if (is_string($result)) {
                    $result = parent::get('$^^$' . $result, $replace, $locale, false);
                    $result = str_replace('$^^$', '', $result);
                }

                if (is_string($result) && request()->has('labelInfo')) {
                    return $key;
                }

                return $result;
            }
        }

        // Get without fallback
        $result = parent::get($key, $replace, $locale, false);

        if ($result === $key) {
            $result = parent::get($key, $replace, $locale, $fallback);
        }

        if (is_string($result) && request()->has('labelInfo')) {
            return $key;
        }

        return $result;
    }

    /**
     * This load all the translations from database
     */
    public static function loadTranslationsFromDatabase()
    {
        if (!self::$_translations) {
            $t = [];
            $translations = Translation::with('language')->get();
            foreach ($translations as $translation) {

                $lang = $translation->language->language;

                if (!isset($t[$lang])) {
                    $t[$lang] = [];
                }

                $t[$lang][($translation->group !== "single" ? $translation->group.'.' : '') . $translation->key] = $translation->value;
            }

            self::$_translations = $t;
        }

        return self::$_translations;
    }
}
